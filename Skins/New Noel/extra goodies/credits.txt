[Credits]
 
 //whereas x = frame number.
------------------------------------------------------------
[MAIN MENU]

dlwnstns {
	Menu-Background,
	Welcome_Text
	}
------------------------------------------------------------
------------------------------------------------------------
[SONG SELECTION] 
 
dlwnstns { 
	Button-(left, middle, right),
    	MenuBack-x,
    	Menu-Button-Background, 
    	Selection-(button), 
	Selection-Mod-(modNames), 
	Selection-Tab,
    	Star, Star2,
	Ranking-(letter)-Small
	} 
 
 
Shouyou {
    	Mode-(mode)-Small,
    	Mode-(mode)-Med,
    	Mode-(mode),
	}
------------------------------------------------------------
------------------------------------------------------------
[RANKING PANEL]

dlwnstns {
	Ranking-(letter),
	Ranking-Perfect,
	Hit(number)
	}

Shouyou {
	Ranking-Accuracy,
	Ranking-Graph,
	Ranking-MaxCombo,
	Ranking-Panel,
	Ranking-Title,
	Ranking-Winner
	}
------------------------------------------------------------
------------------------------------------------------------
[GAMEPLAY]

dlwnstns {
	Arrow-Pause,
	Combo,
	ScoreEntry,
	Cursor,
	Default,
	Fail-Background,
	FollowPoint-x,
	Hit(number)-x
	HitCircle,
	HitCircleOverlay,
	InputOverlay-Background,
	InputOveray-Key,
	Pause-Overlay,
	Pause-(buttons),
	Score,
	ScoreBar-Colour-x,
	ScoreBar-BG,
	Spinner-ApproachCircle,
	Spinner-Background,
	Spinner-Clear,
	Spinner-Metre,
	Spinner-RPM,
	Spinner-Spin
	}

Shouyou {
    	Multi-Skipped,
    	Play-Skip-x,
	}
------------------------------------------------------------